
## About StravaIlao.co


- First of all you should register with /register domain.
- After that you need **Bearer token** and you can bring them using post request api/gettoken with email,password body. 
{
	"email":"mail@mail.com",
	"password":"password"
}
- api/leaderboard (GET) with **Bearer token**  endpoint for "Using Strava API (https://strava.github.io/api/), find the most popular  cycling segments in Istanbul, and check their leaderboards. Return a dictionary of riders only in multiple leaderboards, and how many time they are listed in those leaderboards. (Endpoint 1)" task. 
- api/score (GET) with **Bearer token** endpoint for "Bonus: Calculate a score for each rider listed in those segments and return your highscore list. (Endpoint 2). Be creative and try to be fair with score calculation. Document which variables you are using for score calculation and the score formula" task.


My score calculation formula is sum(100/rank), tell me it is a most creative formula :)

According to me the score is summed according to the rank of the competitor.

elapsed_time is a metric but segment arena is different so i cant include my formula.

