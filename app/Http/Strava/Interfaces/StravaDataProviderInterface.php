<?php


namespace App\Http\Strava\Interfaces;


interface StravaDataProviderInterface
{
    public function getSegments();

    public function getLeaderboardBySegments($segments);

}