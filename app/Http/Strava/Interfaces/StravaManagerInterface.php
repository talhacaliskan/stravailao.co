<?php


namespace App\Http\Strava\Interfaces;


interface StravaManagerInterface
{

    public function getSegments();

    public function getLeaderboardBySegments();

    public function calculateLeaderboardCount();
    public function calculateScore();


}