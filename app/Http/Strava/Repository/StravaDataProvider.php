<?php


namespace App\Http\Strava\Repository;


use App\Http\Strava\Interfaces\StravaDataProviderInterface;
use Mockery\Exception;

class StravaDataProvider implements StravaDataProviderInterface
{
    public function getSegments()
    {
       try{
           $ch = curl_init();

           curl_setopt($ch, CURLOPT_URL, getenv('STRAVA_API_URL').'segments/explore?bounds=40.5660,27.7350,41.8061,30.3607&activity_type=cycling');
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


           $headers = array();
           $headers[] = 'Accept: application/json';
           $headers[] = "Authorization: Bearer ".getenv('STRAVA_TOKEN');
           curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

           $result = curl_exec($ch);
           if (curl_errno($ch)) {
               echo 'Error:' . curl_error($ch);
           }
           $segments = json_decode($result,true);
           foreach ($segments['segments'] as $key => $value) {
               $segments['Istanbul'][]=['name' => $value['name'],'id' => $value['id']];

           }
           unset($segments['segments']);
           curl_close($ch);
           return $segments;
       }
       catch (Exception $exception){
           //log would be nice
           return null;
       }
    }

    public function getLeaderboardBySegments($segments)
    {
        $leadboard=[];
        foreach ($segments['Istanbul'] as $key => $value) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, getenv('STRAVA_API_URL').'segments/'.$value['id'].'/leaderboard');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $headers = array();
            $headers[] = 'Accept: application/json';
            $headers[] = "Authorization: Bearer ".getenv('STRAVA_TOKEN');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            $leaderboard = json_decode($result,true);
            $leadboard[$value['id']] = $leaderboard['entries'];
            unset($leaderboard);
            //var_dump($segments['segments'][0]['name']);
            curl_close($ch);
        }
        return $leadboard;
    }

}