<?php


namespace App\Http\Strava\Services;


use App\Http\Strava\Interfaces\StravaManagerInterface;
use App\Http\Strava\Repository\StravaDataProvider;
use Exception;

class StravaManager implements StravaManagerInterface
{

    private $stravaDataProvider;
    public function __construct()
    {
        $this->stravaDataProvider = new StravaDataProvider();
    }

    public function getSegments()
    {
        try{
            $segments = $this->stravaDataProvider->getSegments();
            return $segments;
        }catch (Exception $exception){
            //log
            return null;
        }
    }

    public function getLeaderboardBySegments()
    {
        try{
            $segments = $this->getSegments();
            if (!is_null($segments)){
                $leaderBoard = $this->stravaDataProvider->getLeaderboardBySegments($segments);
                return $leaderBoard;
            }
        }
        catch (Exception $exception){
            //log
            return null;
        }

    }

    public function calculateLeaderboardCount()
    {
        try{
            $leadboard = $this->getLeaderboardBySegments();
            $list = [];
            foreach ($leadboard as $key => $value) {
                foreach ($value as $newValue) {

                    if(array_key_exists($newValue['athlete_name'],$list)){
                        $list[$newValue['athlete_name']]++;
                    }
                    else{
                        $list[$newValue['athlete_name']] = 1;
                    }
                }
            }

            foreach ($list as $key=>$item) {
                if($item == 1){
                    unset($list[$key]);
                }
            }
            return $list;
        }catch (Exception $exception){
            //log
            return null;
        }
    }
    public function calculateScore()
    {
        try{
            $leadboard = $this->getLeaderboardBySegments();
            $list = [];
            foreach ($leadboard as $key => $value) {
                foreach ($value as $newValue) {
                    if(array_key_exists($newValue['athlete_name'],$list)){
                        $list[$newValue['athlete_name']]['count']++;
                        $list[$newValue['athlete_name']]['score'] += 100/$newValue['rank'];
                    }
                    else{
                        $list[$newValue['athlete_name']] = ['count' => 1,'score' => 100/$newValue['rank']];
                    }
                    $list[$newValue['athlete_name']]['ranks'][] = $newValue['rank'];
                }
            }
            unset($leadboard);
            foreach ($list as $key=>$item) {
                if($item['count'] == 1){
                    unset($list[$key]);
                }
            }
            //echo json_encode($list,JSON_UNESCAPED_UNICODE);
            return $list;
        }catch (Exception $exception){
            //log
            return null;
        }
    }
}