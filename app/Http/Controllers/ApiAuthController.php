<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class ApiAuthController extends Controller
{
    function getToken(Request $request){
      $credentials = $request->only('email','password');
      if(Auth::attempt($credentials)){
          $user = User::where('email',$request->input('email'))->first();
          return json_encode(['Response Code ' => '200','Message (Token)' => $user->api_token ]);
      }
      else{
        return json_encode(['Response Code ' => '500','Message' => 'Failed']);

      }
    }
}
