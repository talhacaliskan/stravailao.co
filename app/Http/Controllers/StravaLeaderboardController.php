<?php

namespace App\Http\Controllers;

use App\Http\Strava\Services\StravaManager;
use Illuminate\Http\Request;

class StravaLeaderboardController extends Controller
{
    public function index()
    {
        $manager = new StravaManager();
        $leaderboard = $manager->calculateLeaderboardCount();
        return json_encode($leaderboard,JSON_UNESCAPED_UNICODE);

    }

    public function score()
    {
        $manager = new StravaManager();
        $scores = $manager->calculateScore();
        return json_encode($scores,JSON_UNESCAPED_UNICODE);;
    }
}
